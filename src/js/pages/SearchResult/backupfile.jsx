import React, { Component } from 'react';
import compose from 'recompose/compose';
// import PropTypes from 'prop-types';
// content section
import Category from "js/components/Content/sections/Category.jsx";
import Cards from "js/components/Content/sections/Cards.jsx";
import withStyles from "@material-ui/core/styles/withStyles";
import Header from "js/components/material-ui-react/Header/Header.jsx";
import Parallax from "js/components/material-ui-react/Parallax/Parallax.jsx";
import HeaderLinks from "js/components/material-ui-react/Header/HeaderLinks.jsx";
import GridContainer from "js/components/material-ui-react/Grid/GridContainer.jsx";
import GridItem from "js/components/material-ui-react/Grid/GridItem.jsx";
import Search from "js/components/AppBar/Search"
// style
import componentsStyle from "assets/jss/material-kit-react/views/components.jsx";
import {getPlaces, getRecommendationPlaces} from 'js/actions/placeActions'
//redux
import {connect} from 'react-redux';

class SearchResultContent extends Component {
  constructor(){
    super();
    this.state ={
      popularlist:[],
      mostriviewed:[],
      activecategory:0,
      selectedLocation:""
    };
  }


  getPlaceList(sortedBy,categoryName) {
    let location = this.state.selectedLocation
    this.setState({activecategory:categoryName});
    this.props.getPlaces(sortedBy,categoryName,location);
  }

  getRecommendationList(sortedBy,categoryName) {
    let location = this.state.selectedLocation
    console.log(location)
    this.props.getRecommendationPlaces(sortedBy,categoryName,location)
  }

  getMostRiviewedList(){
    console.log(this.state.activecategory)
    let param = ""
    if(this.state.activecategory!==""){
      param=this.state.activecategory
    }
    this.getPlaceList("riviewedBy",param);
  }

  componentWillMount(){
    if(this.props.location.state){
        this.setState({selectedLocation:this.props.location.state.locationId})
    }
  }

  componentDidMount(){
    this.getPlaceList();
    this.getMostRiviewedList();
    this.getRecommendationList()
  }

  componentDidUpdate(prevState){
    if(this.state.selectedLocation !== prevState.selectedLocation){
      console.log("Test update location")
    }
  }

  render() {
    let imagePlace;
    const { classes, ...rest } = this.props;
    return (
      <div>
        <Header
          brand="Trevor"
          rightLinks={<HeaderLinks/>}
          fixed
          color="transparent"
          changeColorOnScroll={{
            height: 400,
            color: "white"
          }}
          {...rest}
        />
      <Parallax image={require("assets/img/homepage/homepage4.jpg")}>
          <div className={classes.container}>
            <GridContainer>
              <GridItem>
                <div className={classes.brand}>
                  <h5 className={classes.title}>Mau Kemana Hari Ini?</h5>
                    <div className={classes.typo}>
                      <h2 className={classes.subtitle}>Temukan Tempat Wisata Impianmu</h2>
                    </div>
                </div>
                <div>
                  <Search/>
                </div>
              </GridItem>
            </GridContainer>
          </div>
        </Parallax>
        <Category placeList={this.getPlaceList.bind(this)}/>
        <h2 className={classes.title}>Direkomendasikan Untuk Anda</h2>
        <div className={classes.bodyContent}>
          {
            this.props.recommendation.map((place)=>{
              imagePlace=""
              place.photos.map((prop) => {
                return imagePlace=prop.image
              })
              return <Cards placeId={place.id} placeName={place.name} ratings={place.rating} images={imagePlace} riviewed={place.riviewedBy}/>
            })
          }
        </div>
        <h2 className={classes.title}>Tempat Wista Populer</h2>
        <div className={classes.bodyContent}>
          {
            this.props.places.map((place)=>{
              imagePlace=""
              place.photos.map((prop) => {
                return imagePlace=prop.image
              })
              return <Cards placeId={place.id} placeName={place.name} ratings={place.rating} images={imagePlace} riviewed={place.riviewedBy}/>
            })
          }
        </div>
        <h2 className={classes.title}>Paling Banyak Diriview</h2>
        <div className={classes.bodyContent}>
          {
            this.props.filterresult.map((place)=>{
              imagePlace=""
              place.photos.map((prop) => {
                return imagePlace=prop.image
              })
              return   <Cards placeId={place.id} placeName={place.name} ratings={place.rating} images={imagePlace} riviewed={place.riviewedBy}/>
            })
          }
        </div>
      </div>
    );
  }
}

const mapStateToProp = state => ({
    places: state.places.places,
    filterresult: state.places.filterresult,
    recommendation:state.places.recommendation
});

// export default withStyles(contentStyle)(Content);
export default compose(
  withStyles(componentsStyle, {
    name: 'SearchResultContent',
  }),
  connect(mapStateToProp,{
    getPlaces,getRecommendationPlaces
  }),
)(SearchResultContent);
