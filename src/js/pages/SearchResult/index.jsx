import React, { Component } from 'react';
import {SimpleBottomNavigation} from 'js/components/AppBar'
import classNames from "classnames";
import compose from 'recompose/compose';
import {connect} from 'react-redux';
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Hidden from '@material-ui/core/Hidden';
// Core Component material-ui react
import Header from "js/components/material-ui-react/Header/Header.jsx";
import Parallax from "js/components/material-ui-react/Parallax/Parallax.jsx";
import GridContainer from "js/components/material-ui-react/Grid/GridContainer.jsx";
import GridItem from "js/components/material-ui-react/Grid/GridItem.jsx";
import Search from "js/components/AppBar/Search"
// import Button from "js/components/material-ui-react/CustomButtons/Button.jsx";
// For this section
import SearchResultContent from "js/pages/SearchResult/SearchResultContent.jsx";
import HeaderLinks from "js/components/material-ui-react/Header/HeaderLinks.jsx";
// style
import componentsStyle from "assets/jss/material-kit-react/views/components.jsx";

class SearchResult extends Component {
  render() {
    const { classes, ...rest } = this.props;
    return (
        <div className="App">
          <Header
            brand="Trevor"
            rightLinks={<HeaderLinks/>}
            fixed
            color="transparent"
            changeColorOnScroll={{
              height: 400,
              color: "white"
            }}
            {...rest}
          />
        <Parallax image={require("assets/img/homepage/homepage4.jpg")}>
            <div className={classes.container}>
              <GridContainer>
                <GridItem>
                  <div className={classes.brand}>
                    <h5 className={classes.title}>Mau Kemana Hari Ini?</h5>
                      <div className={classes.typo}>
                        <h2 className={classes.subtitle}>Temukan Tempat Wisata Impianmu</h2>
                      </div>
                  </div>
                  <div>
                    <Search/>
                  </div>
                </GridItem>
              </GridContainer>
            </div>
          </Parallax>
          <div className={classNames(classes.Main,classes.mainRaised)}>
              <SearchResultContent locationId={this.props.location.state.locationId}/>
          </div>
          <Hidden only="lg">
              <SimpleBottomNavigation/>
          </Hidden>
        </div>
    );
  }
}

// export default withStyles(bottomNavigationStyle)(SimpleBottomNavigation);
export default compose(
  withStyles(componentsStyle, {
    name: 'SearchResult',
  }),
  connect(null,{}),
)(SearchResult);
