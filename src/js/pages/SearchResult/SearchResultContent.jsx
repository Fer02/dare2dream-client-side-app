import React, { Component } from 'react';
import compose from 'recompose/compose';
import { Link } from "react-router-dom";
import Button from '@material-ui/core/Button';
// import PropTypes from 'prop-types';
// content section
import Category from "js/components/Content/sections/Category.jsx";
import Cards from "js/components/Content/sections/Cards.jsx";
import withStyles from "@material-ui/core/styles/withStyles";
import GridContainer from "js/components/material-ui-react/Grid/GridContainer.jsx";
import GridItem from "js/components/material-ui-react/Grid/GridItem.jsx";
// style
import contentStyle from "assets/jss/material-kit-react/views/contentStyle.jsx";
import {getPlaces, getRecommendationPlaces, getMorePlaces} from 'js/actions/placeActions'
//redux
import {connect} from 'react-redux';
import {apiconfig} from 'js/api/apiconfig'

class SearchResultContent extends Component {
  constructor(props){
    super(props);
    this.state ={
      popularlist:[],
      mostriviewed:[],
      placeList:[],
      activecategory:0,
      selectedLocation:""
    };
  }


  placeList(sortedBy,categoryName,location) {
    console.log(this.state.activecategory)
    this.setState({activecategory:categoryName})
    this.getShortedPlace(categoryName,"rating",this.state.selectedLocation)
    this.getShortedPlace(categoryName,"riviewedBy",this.state.selectedLocation)
    this.getRecommendationList(null,categoryName,this.state.selectedLocation)
    this.props.getPlaces(this.state.activecategory,null,this.state.selectedLocation)
  }

  getRecommendationList(sortedBy,categoryName,location) {
    console.log(location)
    this.props.getRecommendationPlaces(sortedBy,categoryName,location)
  }

  getMostRiviewedList(location){
    let param = ""
    if(this.state.activecategory!==""){
      param=this.state.activecategory
    }
    this.getPlaceList("riviewedBy",param,location);
  }


  getShortedPlace(param,sortedBy,location){
    if(param===0){
      param="*"
    }
    fetch(apiconfig.base_url('api/place/')+'?sortedBy='+sortedBy+'&location='+location+'&filters='+param)
        .then((result) => {
            return result.json();
        }).then((jsonResult) => {
            if(sortedBy==="rating"){
              this.setState({
                  popularlist: jsonResult.results
              });
            }else if (sortedBy==="riviewedBy") {
              this.setState({
                  mostriviewed: jsonResult.results
              });
            }
        }).catch(() => {
            console.log("<h1>Tempat tidak ditemukan</h1>");
        });
  }

  componentWillMount(){
      this.setState({selectedLocation:this.props.locationId})
  }

  loadContent(location){
    this.getShortedPlace(this.state.activecategory,"rating",location)
    this.getShortedPlace(this.state.activecategory,"riviewedBy",location)
    this.getRecommendationList(null,this.state.activecategory,location)
    this.props.getPlaces(this.state.activecategory,null,location)
  }

  componentDidMount(){
    this.props.getPlaces(null,null,this.state.selectedLocation)
    this.loadContent(this.state.selectedLocation)
  }

  componentWillReceiveProps(nextProps){
    if(this.props.locationId !== nextProps.locationId){
      this.setState({selectedLocation:nextProps.locationId})
      this.loadContent(nextProps.locationId)
    }else if(this.props.places.next!==nextProps.places.next){
      nextProps.places.nextPlaces.map((nextPlace)=>{
          return this.props.places.places.push(nextPlace)
      })
    }
  }

  loadMore = (e) =>{
    e.preventDefault();
    this.props.getMorePlaces(this.props.places.next,null,this.state.selectedLocation)
  }

  // componentDidUpdate(prevState){
  //   if(this.state.activecategory !== prevState.activecategory){
  //
  //   }
  // }

  // shouldComponentUpdate(nextProps, nextState){
  //     if (this.state.activecategory === nextState.activecategory){
  //       return true;
  //     }
  //     return false;
  // }

  render() {
    let imagePlace;
    const { classes } = this.props;
    console.log(this.state.selectedLocation)
    return (
      <div className={classes.container}>
        <GridContainer>
            <GridItem xs={12}>
              <Category placeList={this.placeList.bind(this)}/>
              <h2 className={classes.title}>Direkomendasikan Untuk Anda</h2>
              <Link to={{pathname:'/place/list/recommendation list',state:{locationId:this.state.selectedLocation}}}>
                  <p className={classes.allPlace}>Lihat Semua</p>
              </Link>
            </GridItem>
            <GridItem xs={12}>
              <div className={classes.bodyContent}>
                {
                  this.props.recommendation.map((place)=>{
                    imagePlace=""
                    place.photos.map((prop) => {
                      return imagePlace=prop.image
                    })
                    return <Cards placeId={place.id} placeName={place.name} ratings={place.rating} images={imagePlace} riviewed={place.riviewedBy}/>
                  })
                }
              </div>
            </GridItem>
            <GridItem xs={12}>
              <h2 className={classes.title}>Tempat Wista Populer</h2>
              <Link to={{pathname:'/place/list/popular place',state:{locationId:this.state.selectedLocation}}}>
                  <p className={classes.allPlace}>Lihat Semua</p>
              </Link>
            </GridItem>
            <GridItem xs={12}>
              <div className={classes.bodyContent}>
                {
                  this.state.popularlist.map((place)=>{
                    imagePlace=""
                    place.photos.map((prop) => {
                      return imagePlace=prop.image
                    })
                    return <Cards placeId={place.id} placeName={place.name} ratings={place.rating} images={imagePlace} riviewed={place.riviewedBy}/>
                  })
                }
              </div>
            </GridItem>
            <GridItem xs={12}>
              <h2 className={classes.title}>Paling Banyak Diriview</h2>
              <Link to={{pathname:'/place/list/most reviewed place',state:{locationId:this.state.selectedLocation}}}>
                  <p className={classes.allPlace}>Lihat Semua</p>
              </Link>
            </GridItem>
            <GridItem xs={12}>
              <div className={classes.bodyContent}>
                {
                  this.state.mostriviewed.map((place)=>{
                    imagePlace=""
                    place.photos.map((prop) => {
                      return imagePlace=prop.image
                    })
                    return   <Cards placeId={place.id} placeName={place.name} ratings={place.rating} images={imagePlace} riviewed={place.riviewedBy}/>
                  })
                }
              </div>
            </GridItem>
            <div className={classes.allplacebodyContent}>
              <p className={classes.subHeader}>{this.props.places.count} tempat wisata</p>
              {
                this.props.places.places.map((place)=>{
                  imagePlace=""
                  place.photos.map((prop) => {
                    return imagePlace=prop.image
                  })
                  return   <Cards placeId={place.id} placeName={place.name} ratings={place.rating} images={imagePlace} riviewed={place.riviewedBy}/>
                })
              }
            </div>
            {
              this.props.places.next===""?null:
                  <Button className={classes.button} variant="outlined" size="small" onClick={this.loadMore}>
                    Load More
                  </Button>
            }
          </GridContainer>
      </div>
    );
  }
}

const mapStateToProp = state => ({
    places: state.places,
    filterresult: state.places.filterresult,
    recommendation:state.places.recommendation
});

// export default withStyles(contentStyle)(Content);
export default compose(
  withStyles(contentStyle, {
    name: 'SearchResultContent',
  }),
  connect(mapStateToProp,{
    getPlaces,getRecommendationPlaces,getMorePlaces
  }),
)(SearchResultContent);
