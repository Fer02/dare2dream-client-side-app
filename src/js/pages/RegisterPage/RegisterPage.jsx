import React from 'react';
import compose from 'recompose/compose';
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// import InputAdornment from "@material-ui/core/InputAdornment";
// @material-ui/icons
// import Email from "@material-ui/icons/Email";
// import LockOutline from "@material-ui/icons/LockOutline";
// import People from "@material-ui/icons/People";
// core components
import Header from "js/components/material-ui-react/Header/Header.jsx";
// import HeaderLinks from "js/components/material-ui-react/Header/HeaderLinks.jsx";
import GridContainer from "js/components/material-ui-react/Grid/GridContainer.jsx";
import GridItem from "js/components/material-ui-react/Grid/GridItem.jsx";
import Button from "js/components/material-ui-react/CustomButtons/Button.jsx";
// import Card from "js/components/material-ui-react/Card/Card.jsx";
// import CardBody from "js/components/material-ui-react/Card/CardBody.jsx";
// import CardHeader from "js/components/material-ui-react/Card/CardHeader.jsx";
// import CardFooter from "js/components/material-ui-react/Card/CardFooter.jsx";
// import CustomInput from "js/components/material-ui-react/CustomInput/CustomInput.jsx";

import loginPageStyle from "assets/jss/material-kit-react/views/loginPage.jsx";
// import { Link } from "react-router-dom";
// import image from "assets/img/login-bg.jpg";
import logo from "assets/img/logo/HeaderLogoV2.png"
// redux
import {connect} from 'react-redux';
import { createUser } from 'js/actions/userActions';
import { TextField } from '@material-ui/core';


class RegisterPage extends React.Component {
  constructor(props) {
    super(props);
    // we use this to make the card to appear after the page has been rendered
    this.state = {
      cardAnimaton: "cardHidden",
      first_name: '',
      last_name: '',
      email:'',
      password:''
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  componentDidMount() {
    // we add a hidden class to the card and after 700 ms we delete it and the transition appears
    setTimeout(
      function() {
        this.setState({ cardAnimaton: "" });
      }.bind(this),
      700
    );
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();

    const user = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      email: this.state.email,
      password: this.state.password
    };
    this.props.createUser(user,this.props.history);
  }

  render() {
    const { classes, ...rest } = this.props;
    return (
      <div>
        <Header
          absolute
          color="white"
          brand="Register"
          {...rest}
        />
          <div className={classes.container}>
            <GridContainer justify="center">
              <GridItem xs={12} sm={12} md={4}>
                  <form className={classes.form} onSubmit={this.onSubmit}>
                    <div className={classes.socialLine}>
                            <img src={logo} alt="Logo" className={classes.photoLogo}></img>
                      </div>
                      <TextField
                      className={classes.textfield}
                        placeholder="First Name"
                        id="first"
                        formControlProps={{
                          fullWidth: true
                        }}
                        Inputprops={{
                          type: "text",
                          name:"first_name",
                          onChange:(this.onChange),
                          value:(this.state.first_name),
                          disableUnderline: true,
                        }}

                      />
                      <TextField
                        className={classes.textfield}
                        placeholder="Last Name"
                        id="Last"
                        formControlProps={{
                          fullWidth: true
                        }}
                        Inputprops={{
                          type: "text",
                          name:"last_name",
                          onChange:(this.onChange),
                          value:(this.state.last_name),
                          disableUnderline: true
                        }}
                      />
                      <TextField
                      className={classes.textfield}
                        placeholder="Email"
                        id="email"
                        formControlProps={{
                          fullWidth: true
                        }}
                        Inputprops={{
                          type: "email",
                          name:"email",
                          onChange:(this.onChange),
                          value:(this.state.email),
                          disableUnderline: true
                        }}
                      />
                      <TextField
                        className={classes.textfield}
                        placeholder="Password"
                        id="pass"
                        formControlProps={{
                          fullWidth: true
                        }}
                        Inputprops={{
                          type: "password",
                          name:"password",
                          onChange:(this.onChange),
                          value:(this.state.password),
                          disableUnderline: true
                        }}
                      />
                      <Button type="submit" className={classes.submitButton} size="lg">
                        Get started
                      </Button>
                  </form>
              </GridItem>
            </GridContainer>
          </div>
        </div>
    );
  }
}

RegisterPage.propTypes = {
  createUser: PropTypes.func.isRequired
};

export default compose(
  withStyles(loginPageStyle, {
    name: 'RegisterPage',
  }),
  connect(null,{
    createUser
  }),
  withRouter
)(RegisterPage);
