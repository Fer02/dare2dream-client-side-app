import React, { Component } from 'react';
import Layout from 'js/components/Layout';
import blue from '@material-ui/core/colors/blue';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import {connect} from 'react-redux';

const theme = createMuiTheme({
  palette: {
    primary: blue
  }
});

class HomePage extends Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <div>
          <Layout/>
        </div>
      </MuiThemeProvider>
    );
  }
}

const mapStateToProps = state => ({
  session:state.session
});

export default connect(mapStateToProps,{})(HomePage);
