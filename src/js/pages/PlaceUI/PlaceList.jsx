import React from 'react';
import compose from 'recompose/compose';
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";
import classNames from "classnames";
// @material-ui/core components
import Category from "js/components/Content/sections/Category.jsx";
import withStyles from "@material-ui/core/styles/withStyles";
import Hidden from '@material-ui/core/Hidden';
import Footer from "js/components/material-ui-react/Footer/Footer.jsx";
import Header from "js/components/material-ui-react/Header/Header.jsx";
import HeaderLinks from "js/components/material-ui-react/Header/HeaderLinks.jsx";
import Cards from "js/components/Content/sections/Cards.jsx";

import GridContainer from "js/components/material-ui-react/Grid/GridContainer.jsx";
import GridItem from "js/components/material-ui-react/Grid/GridItem.jsx";
// import Button from "js/components/material-ui-react/CustomButtons/Button.jsx";
import {SimpleBottomNavigation} from 'js/components/AppBar'
import placeListPageStyle from "assets/jss/material-kit-react/views/placeListPage.jsx";
import {getPlaces} from 'js/actions/placeActions'
// import { Link } from "react-router-dom";
// redux
import {connect} from 'react-redux';

class PlaceList extends React.Component {
  constructor(props){
    super(props);
    this.state ={
      sortedBy:"",
      activecategory:0,
      selectedLocation:""
    };
  }

  placeList(categoryName) {
    this.setState({activecategory:categoryName});
    this.props.getPlaces(this.state.sortedBy,this.state.activecategory,this.state.selectedLocation);
  }

  componentWillMount(){
    this.setState({selectedLocation:this.props.location.state.locationId})
    if(this.props.match.params.type === "popular place"){
        this.setState({sortedBy:"rating"})
    }else if (this.props.match.params.type === "most reviewed place") {
        this.setState({sortedBy:"riviewedBy"})
    }
  }

  componentDidMount(){
    console.log(this.state.selectedLocation)
    this.props.getPlaces(this.state.sortedBy,this.state.activecategory,this.state.selectedLocation);
  }


  render() {
    let imagePlace;
    const { classes, ...rest } = this.props;
    console.log()
    const popular =
        this.props.filterresult.map((place)=>{
          imagePlace=""
          place.photos.map((prop) => {
            return imagePlace=prop.image
          })
          return <Cards placeId={place.id} placeName={place.name} ratings={place.rating} images={imagePlace} riviewed={place.riviewedBy}/>
        })

    const mostreviewed =
      this.props.filterresult.map((place)=>{
        imagePlace=""
        place.photos.map((prop) => {
          return imagePlace=prop.image
        })
        return <Cards placeId={place.id} placeName={place.name} ratings={place.rating} images={imagePlace} riviewed={place.riviewedBy}/>
      })
    const recommendation =
      this.props.recommendation.map((place)=>{
        imagePlace=""
        place.photos.map((prop) => {
          return imagePlace=prop.image
        })
        return <Cards placeId={place.id} placeName={place.name} ratings={place.rating} images={imagePlace} riviewed={place.riviewedBy}/>
      })
    return (
      <div>
        <Header
          brand="Trevor"
          rightLinks={<HeaderLinks/>}
          fixed
          color="white"
          changeColorOnScroll={{
            height: 400,
            color: "white"
          }}
          {...rest}
        />
      <div className={classNames(classes.main, classes)}>
          <div className={classes.placeCategory}>
            <Category placeList={this.placeList.bind(this)}/>
          </div>
          <div className={classes.container}>
            <div className={classes.allplacebodyContent}>
              <GridContainer className={classes.list}>
                  <GridItem xs={12} sm={12} md={12}>
                    {this.props.match.params.type === "popular place" ? popular:null}
                    {this.props.match.params.type === "most reviewed place" ? mostreviewed:null}
                    {this.props.match.params.type === "recommendation list" ? recommendation:null}
                  </GridItem>
              </GridContainer>
            </div>
          </div>
        </div>
        <Hidden only="lg">
            <SimpleBottomNavigation/>
        </Hidden>
        <Hidden only={['xs','sm','md']}>
          <div className={classes.footer}>
              <Footer />
          </div>
        </Hidden>
      </div>
    );
  }
}

PlaceList.propTypes = {
  createUser: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    places: state.places.places,
    filterresult: state.places.filterresult,
    recommendation:state.places.recommendation
});

export default compose(

  withStyles(placeListPageStyle, {
    name: 'PlaceList',
  }),
  connect(mapStateToProps,{getPlaces}),
  withRouter
)(PlaceList);
