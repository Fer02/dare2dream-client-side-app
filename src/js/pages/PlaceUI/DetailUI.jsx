import React from 'react';
import compose from 'recompose/compose';
// import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";
import classNames from "classnames";
import { Link } from "react-router-dom";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// import InputAdornment from "@material-ui/core/InputAdornment";
// @material-ui/icons
// import Email from "@material-ui/icons/Email";
// import Settings from "@material-ui/icons/Settings";
// core components
import StarRatingComponent from 'react-star-rating-component';
// import Header from "js/components/material-ui-react/Header/Header.jsx";
// import HeaderLinks from "js/components/material-ui-react/Header/HeaderLinks.jsx";
import GridContainer from "js/components/material-ui-react/Grid/GridContainer.jsx";
import GridItem from "js/components/material-ui-react/Grid/GridItem.jsx";
// import Button from "js/components/material-ui-react/CustomButtons/Button.jsx";
// import {SimpleBottomNavigation} from 'js/components/AppBar'
import FormReview from 'js/components/Review/FormReview.jsx'
import detailPageStyle from "assets/jss/material-kit-react/views/detailPage.jsx";
// import { Link } from "react-router-dom";
// redux
import {connect} from 'react-redux';
import {getDetailPlace} from 'js/actions/placeActions'
import SectionCarousel from 'js/components/Content/sections/SectionCarousel.jsx'
import CustomTabs from "js/components/material-ui-react/CustomTabs/CustomTabs.jsx";
// import SingleLineGridList from 'js/components/Content/sections/SingleGridList.jsx'
import {apiconfig} from 'js/api/apiconfig'
import profile from "assets/img/faces/default-avatar.jpg";

class DetailUI extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      placeid:"",
      riviews:[],
      isReviewd:false
    };
  }

  getReviewLists() {
      fetch(apiconfig.base_url('api/review/')+this.props.location.state.placeId)
          .then((result) => {
              return result.json();
          }).then((jsonResult) => {
              this.setState({
                  riviews: jsonResult.results
              });
          }).catch(() => {
              console.log("<h1>Mohon belum ada riview</h1>");
          });
  }

  componentWillMount(){
    if(this.state.placeid===""){
      if(this.props.location.state){
          this.setState({placeid:this.props.location.state.placeId})
      }
    }
  }

  componentDidMount(){
    this.props.getDetailPlace(this.state.placeid);
    this.getReviewLists();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.newReviews) {
      this.getReviewLists();
    }
  }

  render() {
    const { classes } = this.props;
    console.log(this.state.riviews)
    const imageClasses = classNames(
      classes.imgRaised,
      classes.imgRoundedCircle,
      classes.imgReview
    );
    const riviewlist= this.state.riviews!=null ?
    this.state.riviews.map(riview=>(
      <div className={classes.reviewContent}>
        <GridContainer justify="center">
          <GridItem xs={2} sm={2} md={2}>
              <img src={riview.image!=null? apiconfig.base_url("static/img/user/")+riview.image: profile} alt="..." className={imageClasses}  />
          </GridItem>
          <GridItem xs={10} sm={10} md={10} className={classes.content}>
            <p><strong>{riview.name}</strong></p>
            <div className={classes.ratingInfo}>
                <p>({riview.rating})</p>
              <StarRatingComponent
                name="rate2"
                editing={false}
                starCount={5}
                value={riview.rating}
              />
            </div>
            <p>{riview.comment}</p>
          </GridItem>
        </GridContainer>
      </div>
    )) : <p>No reviews submitted</p>

    return (
      <div className={classes.mainDetail}>
        {/* <Header
          brand="Trevor"
          rightLinks={<HeaderLinks/>}
          fixed
          color="white"
          changeColorOnScroll={{
            height: 400,
            color: "white"
          }}
          {...rest}
        /> */}
      <div className={classNames(classes.main, classes)}>
          <div className={classes.container}>
              <GridContainer justify="center">
                  <GridItem xs={12} sm={6} md={6} className={classes.placeGalery}>
                    <SectionCarousel/>
                  </GridItem>
                  <GridItem xs={12} sm={6} md={6}>
                    <div className={classes.infoContainer}>
                      <CustomTabs
                        plainTabs
                        headerColor="primary"
                        tabs={[
                          {
                            tabName: "Overview",
                            tabContent: (
                              <div className={classes.infoContent}>
                                <h3 className={classes.title}>{this.props.match.params.placeName}</h3>
                                <div className={classes.ratingInfo}>
                                  <p>({this.props.place.rating})</p>
                                  <StarRatingComponent
                                    name="rate2"
                                    editing={false}
                                    starCount={5}
                                    value={this.props.place.rating}
                                  />
                                </div>
                                <div className={classes.mainInfo}>
                                  <Link to={'/'}>
                                      <p>{this.props.place.riviewedBy} reviews</p>
                                  </Link>
                                  <p>{this.props.place.description}</p>
                                  <p><strong>Alamat :</strong>{this.props.place.address}</p>
                                </div>
                              </div>
                            )
                          },
                          {
                            tabName: "Reviews",
                            tabContent: (
                              <div className={classes.infoContent}>
                                <div className={classes.mainInfoRiviews}>
                                    <FormReview placeId={this.state.placeid} placeName={this.props.match.params.placeName} address={this.props.place.address}/>
                                    {riviewlist}
                                </div>
                              </div>
                            )
                          }
                        ]}
                      />
                    </div>
                  </GridItem>
              </GridContainer>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  place:state.places.place,
  newReviews:state.review.newreviews
});

export default compose(

  withStyles(detailPageStyle, {
    name: 'DetailUI',
  }),
  connect(mapStateToProps,{
    getDetailPlace
  }),
  withRouter
)(DetailUI);
