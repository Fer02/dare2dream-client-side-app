import React from 'react';
import compose from 'recompose/compose';
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// import InputAdornment from "@material-ui/core/InputAdornment";
// @material-ui/icons
// import Email from "@material-ui/icons/Email";
// import LockOutline from "@material-ui/icons/LockOutline";
// core components
import Hidden from '@material-ui/core/Hidden';
import Header from "js/components/material-ui-react/Header/Header.jsx";
// import HeaderLinks from "js/components/material-ui-react/Header/HeaderLinks.jsx";
import GridContainer from "js/components/material-ui-react/Grid/GridContainer.jsx";
import GridItem from "js/components/material-ui-react/Grid/GridItem.jsx";
import Button from "js/components/material-ui-react/CustomButtons/Button.jsx";
// import CustomInput from "js/components/material-ui-react/CustomInput/CustomInput.jsx";
import TextField from '@material-ui/core/TextField';

import loginPageStyle from "assets/jss/material-kit-react/views/loginPage.jsx";
import { Link } from "react-router-dom";
import logo from "assets/img/logo/HeaderLogoV2.png"
// redux
import {connect} from 'react-redux';
import { loginUser } from 'js/actions/sessionActions';


const mapStateToProps = state => ({
  session:state.session
});

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    // we use this to make the card to appear after the page has been rendered
    this.state = {
      cardAnimaton: "cardHidden",
      email: '',
      password: ''
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

  }

  componentDidMount() {
    // we add a hidden class to the card and after 700 ms we delete it and the transition appears
    setTimeout(
      function() {
        this.setState({ cardAnimaton: "" });
      }.bind(this),
      700
    );
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e){
    e.preventDefault();

    const credentials = {
      email: this.state.email,
      password: this.state.password
    };
    this.props.loginUser(credentials)
    // .then(
    //   (res)=> this.props.history.push('/'),
    //   (err) => console.log(err)
    // );
  }

  render() {
    const { classes, ...rest } = this.props;
    const alert=
      <p style={{color:"red"}}>{this.props.session.alert.alerts}</p>
    return (
      <div>

        <Header
          absolute
          color="white"
          brand={
            <p>Sign In</p>
          }
          {...rest}
        />
            <div className={classes.container}>
              <GridContainer justify="center">
                <GridItem xs={12} sm={12} md={4}>
                    <form className={classes.form} onSubmit={this.onSubmit}>
                        <div className={classes.socialLine}>
                              <img src={logo} alt="Logo" className={classes.photoLogo}></img>
                        </div>
                            <TextField
                              className={classes.textfield}
                              placeholder="Email"
                              id="email"
                              formControlProps={{
                                fullWidth: true,
                                marginNormal:"normal"
                              }}
                              InputProps={{
                                type: "email",
                                name:"email",
                                onChange:(this.onChange),
                                value:(this.state.email),
                                disableUnderline: true
                                // endAdornment: (
                                //   <InputAdornment position="end">
                                //     <Email className={classes.inputIconsColor} />
                                //   </InputAdornment>
                                // )
                              }}
                            />
                            {alert}
                            <TextField
                              className={classes.textfield}
                              placeholder="Password"
                              id="pass"
                              formControlProps={{
                                fullWidth: true
                              }}
                              InputProps={{
                                type: "password",
                                name:"password",
                                onChange:(this.onChange),
                                value:(this.state.password),
                                disableUnderline: true
                                // endAdornment: (
                                //   <InputAdornment position="end">
                                //     <LockOutline
                                //       className={classes.inputIconsColor}
                                //     />
                                //   </InputAdornment>
                                // )
                              }}
                            />
                           {alert}
                            <Button
                            type="submit"
                            className={classes.submitButton}
                            size="lg">
                                Sign In
                            </Button>
                            <p className={classes.detail}>Forgot your Detail?</p>
                        </form>
                        <Hidden mdUp>
                          <Link to={"/register"}>
                            <Button
                              color="white"
                              className={classes.createNewAccountButton}
                              >
                                Create New Account
                            </Button>
                          </Link>
                        </Hidden>
                        <Hidden smDown>
                          <Link to={"/register"}>
                            <center><p className={classes.createNewAccountButton}>Create New Account</p></center>
                          </Link>
                        </Hidden>
                </GridItem>
              </GridContainer>
            </div>
        </div>
    );
  }
}

LoginPage.propTypes = {
  loginUser: PropTypes.func.isRequired
};

export default compose(
  withStyles(loginPageStyle, {
    name: 'LoginPage',
  }),
  connect(mapStateToProps,{
    loginUser
  }),
  withRouter
)(LoginPage);
