import React from 'react';
import compose from 'recompose/compose';
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";
import classNames from "classnames";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Hidden from '@material-ui/core/Hidden';
import Footer from "js/components/material-ui-react/Footer/Footer.jsx";
// import InputAdornment from "@material-ui/core/InputAdornment";
// @material-ui/icons
// import Email from "@material-ui/icons/Email";
// import LockOutline from "@material-ui/icons/LockOutline";
// import Settings from "@material-ui/icons/Settings";
// core components
import Header from "js/components/material-ui-react/Header/Header.jsx";
import HeaderLinks from "js/components/material-ui-react/Header/HeaderLinks.jsx";
import GridContainer from "js/components/material-ui-react/Grid/GridContainer.jsx";
import GridItem from "js/components/material-ui-react/Grid/GridItem.jsx";
// import Button from "js/components/material-ui-react/CustomButtons/Button.jsx";
import {SimpleBottomNavigation} from 'js/components/AppBar'
import profilePage from "assets/jss/material-kit-react/views/profilePage.jsx";
// import { Link } from "react-router-dom";
// redux
import {connect} from 'react-redux';

import profile from "assets/img/faces/default-avatar.jpg";

class ProfilePage extends React.Component {
  render() {
    let image_profile;
    const { classes, ...rest } = this.props;
    const imageClasses = classNames(
      classes.imgRaised,
      classes.imgRoundedCircle,
      classes.imgFluid
    );

    console.log(this.props.user.image)
    if(this.props.user.image){
        image_profile= <img src={"http://127.0.0.1:8080/static/img/user/"+this.props.user.image} alt="..." className={imageClasses} />
    }else{
        image_profile= <img src={profile} alt="..." className={imageClasses} />
    }

    return (
      <div>
        <Header
          brand="Trevor"
          rightLinks={<HeaderLinks/>}
          fixed
          color="white"
          changeColorOnScroll={{
            height: 400,
            color: "primary"
          }}
          {...rest}
        />
      <div className={classNames(classes.main, classes)}>
          <div className={classes.container}>
            <div>
              <GridContainer justify="center">
                  <GridItem xs={12} sm={12} md={4}>
                  <div className={classes.profile}>
                    <div>
                        {image_profile}
                    </div>
                    <div className={classes.name}>
                      <h3 className={classes.title}>{this.props.user.first_name} {this.props.user.last_name}</h3>
                      <p>{this.props.user.email}</p>
                    </div>
                  </div>
                  </GridItem>
              </GridContainer>
            </div>
          </div>
        </div>
        <Hidden only="lg">
            <SimpleBottomNavigation/>
        </Hidden>
        <Hidden only={['xs','sm','md']}>
          <div className={classes.footer}>
              <Footer />
          </div>
        </Hidden>
      </div>
    );
  }
}

ProfilePage.propTypes = {
  createUser: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  user:JSON.parse(localStorage.getItem('user')),
});

export default compose(
  withStyles(profilePage, {
    name: 'ProfilePage',
  }),
  connect(mapStateToProps,{}),
  withRouter
)(ProfilePage);
