import {types} from 'js/actions/types';

const initialState = {
  locations:[],
}

export default function(state = initialState, action){
  let out = state
  switch (action.type) {
    case types.LOCATION_FULFILLED:
      out={
        ...state,
        locations:action.payload.results
      }
      break
    default:
      out={...state}
      break
  }
  return out
}
