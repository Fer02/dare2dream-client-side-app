import {types} from 'js/actions/types';

const initialState = {
  riviews:[],
  newreviews:[],
  review:{},
  alert: {
      title: "",
      show: false,
      type: null,
      alerts: ""
  }
}

export default function(state = initialState, action){
  let out = state
  switch (action.type) {
    case types.POST_REVIEW_FULFILLED:
      out={
        ...state,
        newreviews:action.payload.results,
        alert:{
          title: "Success",
          show: true,
          type: null,
          alerts: "Thank you for your reviews"
        }
      }
      break
    default:
      out={...state}
      break
  }
  return out
}
