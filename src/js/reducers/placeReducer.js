import {types} from 'js/actions/types';

const initialState = {
  places:[],
  param:{},
  filterresult:[],
  riviews:[],
  review:{},
  place:[],
  categories:[],
  category:{},
  recommendation:[],
  popularplace:[],
  mostreviewed:[],
  nextPlaces:[],
  count:{},
  next:{},
  prev:{},
}

export default function(state = initialState, action){
  let out = state
  switch (action.type) {
    case types.PLACE_CATEGORIES_FULFILLED:
      out={
        ...state,
        categories:action.payload.categories
      }
      break
    case types.PLACE_FULFILLED:
      out={
        ...state,
        count:action.payload.count,
        places:action.payload.results,
        next:action.payload.next,
        prev:action.payload.previous
      }
      break
    case types.POPULAR_PLACE_FULFILLED:
      out={
        ...state,
        popularplace:action.payload.results,
      }
      break
    case types.MOSTREVIEWED_PLACE_FULFILLED:
      out={
        ...state,
        mostreviewed:action.payload.results,
      }
      break
    case types.LOADMORE_PLACE_FULFILLED:
      out={
        ...state,
        count:action.payload.count,
        nextPlaces:action.payload.results,
        next:action.payload.next,
        prev:action.payload.previous
      }
      break
    case types.PLACE_FILTER_FULFILLED:
      out={
        ...state,
        param:action.payload.param,
        filterresult:action.payload.results
      }
      break
    case types.PLACE_DETAIL_FULFILLED:
      out={
        ...state,
        place:action.payload.details['0'],
      }
      break
    case types.PLACE_RECOMMENDATION_FULFILLED:
      out={
        ...state,
        recommendation:action.payload.results,
      }
      break
    case types.PLACE_LIKED_FULFILLED:
      out={
        ...state,
      }
      break
    default:
      out={...state}
      break
  }
  return out
}
