import {types} from 'js/actions/types';

const initialState = {
  loggedIn:false,
  title: null,
  alert: {
      title: "",
      show: false,
      type: null,
      alerts: ""
  },
  registering: true,
  login: false,
  login_redirect: false,
  loading: false,
  user:{},
}

export default function(state = initialState, action){
  let out = state;
  switch (action.type) {
    case types.USER_LOGIN_FULFILLED:
      console.log(action)
      localStorage.setItem('jwt', JSON.stringify(action.payload.token))
      localStorage.setItem('user', JSON.stringify(action.payload.user))
      out = {
        ...state,
        loggedIn:true,
        login:true,
      }
      break
    case types.USER_LOGIN_REJECTED:
      out= {
        alert:{
          title: "Opps",
          show: true,
          type: null,
          alerts: "Please check your email and password"
        }
      }
      break
    case types.USER_LOGIN_PENDING: {
        out = { ...state, login: true }
        break
    }
    case types.NEW_USER_FULFILLED:
      localStorage.setItem('user',JSON.stringify(action.payload.result))
      out={
        ...state,
        registering:true,
        loggedIn:true,
        login:true,
        user:action.payload.result
      }
      break
    case types.CHANGE_LOGIN:
        if(!action.payload){
          localStorage.removeItem('user')
          localStorage.removeItem('jwt')
        }
        out = { ...state, loggedIn: action.payload }
        break
    default:
      out={...state}
      break
  }
  return out
}
