import {combineReducers} from 'redux';
import userReducer from './userReducer';
import sessionReducer from './sessionReducer';
import placeReducer from './placeReducer';
import locationReducer from './locationReducer';
import reviewReducer from './reviewReducer';

export default combineReducers({
  users: userReducer,
  session:sessionReducer,
  places:placeReducer,
  locations:locationReducer,
  review:reviewReducer
});
