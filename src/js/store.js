import {createStore,applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
// import {createStore,applyMiddleware,compose} from 'redux';
// import logger from 'redux-logger';
import rootReducer from './reducers';
import { routerMiddleware } from 'react-router-redux';
import { createHashHistory } from 'history';
import promise from 'redux-promise-middleware'

const initialState={};

const reduxRouterMiddleware = routerMiddleware(createHashHistory());

const middleware = [thunk,reduxRouterMiddleware];

const store= createStore(
  rootReducer,
  initialState,
  applyMiddleware(...middleware,promise()),
  // compose(
  //     applyMiddleware(...middleware,logger,promise()),
  //     window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  // )
);

export default store
