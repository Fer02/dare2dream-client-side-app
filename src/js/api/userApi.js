import {apiconfig} from './apiconfig'

class UserApi {
  static register(postData){
    const request = new Request(apiconfig.base_url('api/user/register'),{
        method:'POST',
        headers: new Headers({
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Headers': 'Content-Type',
          'Access-Control-Allow-Methods': 'POST',
          'content-type': 'application/json'
        }),
        body: JSON.stringify({user:postData})
    })
    return fetch(request).then(response=>{
      return response.json();
    }).catch(error=>{
      return error;
    });
  }
}

export default UserApi;
