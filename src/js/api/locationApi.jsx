import {apiconfig} from './apiconfig'

class LocationApi {
  static getLocationList(param,category){
    console.log(category)
    const request = new Request(apiconfig.base_url('api/location'),{
        method:'GET'
    })
    return fetch(request).then(response=>{
      return response.json();
    }).catch(error=>{
      return error;
    });
  }
}

export default LocationApi;
