import {apiconfig} from './apiconfig'

class ReviewApi {
  static reviews(param){
    var temp=JSON.parse(localStorage.getItem('user'));
    var current_user=temp.email;
    param["user_id"]=current_user;
    console.log(param)
    const request = new Request(apiconfig.base_url('api/review/'),{
        method:'POST',
        headers: new Headers({
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Headers': 'Content-Type',
          'Access-Control-Allow-Methods': 'POST',
          'content-type': 'application/json'
        }),
        body: JSON.stringify({reviews:param})
    })
    return fetch(request).then(response=>{
      console.log()
      if(response.ok){
          return response.json();
      }else{
        throw Promise.reject(response.statusText)
      }
    }).catch(error=>{
      return error;
    });
  }
}
export default ReviewApi;
