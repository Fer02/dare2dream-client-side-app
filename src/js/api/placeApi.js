import {apiconfig} from './apiconfig'

class PlaceApi {
  static getPlaceList(param,category,location){
    console.log(param)
    console.log(location)
    console.log(category)
    const request = new Request(apiconfig.base_url('api/place')+'?sortedBy='+param+'&filters='+category+'&location='+location,{
        method:'GET'
    })
    return fetch(request).then(response=>{
      console.log(response)
      return response.json();
    }).catch(error=>{
      return error;
    });
  }

  static getMorePlaceList(nextUrl,prevUrl,location){
    const request = new Request(apiconfig.base_url(nextUrl)+'&location='+location,{
        method:'GET'
    })
    return fetch(request).then(response=>{
      return response.json();
    }).catch(error=>{
      return error;
    });
  }

  static getRecommendationList(param,category,location){
    var temp=JSON.parse(localStorage.getItem('user'));
    var current_user=temp.email;
    const request = new Request(apiconfig.base_url('api/place/recommendation')+'?sortedBy='+param+'&filters='+category+'&current_user='+current_user+'&location='+location,{
        method:'GET'
    })
    return fetch(request).then(response=>{
      return response.json();
    }).catch(error=>{
      return error;
    });
  }

  static getCategoriesList(){
    const request= new Request(apiconfig.base_url('api/place/category'),{
      method:'GET'
    })
    return fetch(request).then(response=>{
      return response.json()
    }).catch(error=>{
      return error;
    });
  }

  static getDetailPlace(placeid){
    console.log(placeid)
    const request= new Request(apiconfig.base_url('api/place/')+placeid,{
      method:'GET'
    })
    return fetch(request).then(response=>{
      return response.json()
    }).catch(error=>{
      return error;
    });
  }

  static like(postData){
    const request = new Request(apiconfig.base_url('api/place/like'),{
        method:'POST',
        headers: new Headers({
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Headers': 'Content-Type',
          'Access-Control-Allow-Methods': 'POST',
          'content-type': 'application/json'
        }),
        body: JSON.stringify({like:postData})
    })
    return fetch(request).then(response=>{
      if(response.ok){
          return response.json();
      }else{
        throw Promise.reject(response.statusText)
      }
    }).catch(error=>{
      return error;
    });
  }
}

export default PlaceApi;
