import {apiconfig} from './apiconfig'

//
class SessionApi {
  static login(postData){
    const request = new Request(apiconfig.base_url('api/login'),{
        method:'POST',
        headers: new Headers({
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Headers': 'Content-Type',
          'Access-Control-Allow-Methods': 'POST',
          'content-type': 'application/json'
        }),
        body: JSON.stringify({auth:postData})
    })
    return fetch(request).then(response=>{
      console.log()
      if(response.ok){
          return response.json();
      }else{
        throw Promise.reject(response.statusText)
      }
    }).catch(error=>{
      return error;
    });
  }
}

export default SessionApi;
