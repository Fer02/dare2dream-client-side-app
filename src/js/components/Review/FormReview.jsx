import React from 'react';
import PropTypes from 'prop-types';
import RateReview from "@material-ui/icons/RateReview";
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import StarRatings from 'react-star-ratings';
import reviewFormStyle from "assets/jss/material-kit-react/components/reviewFormStyle.jsx";
import {postReviews} from 'js/actions/reviewActions'
import compose from 'recompose/compose';
import { withRouter } from "react-router-dom";
import {connect} from 'react-redux';

class FormReview extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
        rating: 0,
        open:false,
        comment:"",
        hasError:false

      };
      this.changeRating=this.changeRating.bind(this)
      this.onChange = this.onChange.bind(this);
      this.onSubmit = this.onSubmit.bind(this);
  }

  changeRating(newRating, name) {
    this.setState({
      rating: newRating
     });
   }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  onStarClick(nextValue, prevValue, name) {
    this.setState({rating: nextValue});
  }

  onSubmit=(e)=>{
    e.preventDefault()
    const review = {
      rating: this.state.rating,
      comment: this.state.comment,
      placeid:this.props.placeId
    };
    console.log(review);
    this.props.postReviews(review);
    this.setState({open:false});
  }

  onChange=(e)=>{
    this.setState({ [e.target.name]: e.target.value });
  }

  componentDidCatch(){
    this.setState({hasError:true})
  }

  render() {
    const { classes } = this.props;
    console.log(this.state.hasError)
    return (
      <div className={classes.main}>
        <Button className={classes.button} variant="contained" size="small" onClick={this.handleOpen}>
          <RateReview/>
          Reviews
        </Button>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div className={classes.paper}>
            <form onSubmit={this.onSubmit}>
                <Typography variant="title" id="modal-title">
                  {this.props.placeName}
                  <p>{this.props.address}</p>
                </Typography>
                <div className={classes.reviewContent}>
                   <StarRatings
                     rating={this.state.rating}
                     changeRating={this.changeRating}
                     starRatedColor="orange"
                     starHoverColor="orange"
                     numberOfStars={5}
                     starDimension="40px"
                     name='rating'
                   />
                   <TextField
                       id="textarea"
                       name="comment"
                       placeholder="Bagaimana pengalamanmu di tempat ini ?"
                       multiline
                       className={classes.textField}
                       margin="normal"
                       onChange={this.onChange}
                       value={this.state.comment}
                     />
              </div>
              {
                this.state.rating===0?<Button disabled type="submit">Post</Button>:<Button type="submit">Post</Button>
              }
              <Button onClick={this.handleClose}>Cancel</Button>
            </form>
          </div>
        </Modal>
      </div>
    );
  }
}

FormReview.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(
  withStyles(reviewFormStyle, {
    name: 'FormReview',
  }),
  connect(null,{
    postReviews
  }),
  withRouter
)(FormReview);
