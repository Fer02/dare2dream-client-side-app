import React from 'react';
import { Link } from "react-router-dom";
import compose from 'recompose/compose';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
//@material-ui/icons
import HomeIcon from '@material-ui/icons/Home';
import AccountCircle from '@material-ui/icons/AccountCircle'
import Paper from '@material-ui/core/Paper'
import MailOutline from '@material-ui/icons/MailOutline'
import ImportContacts from '@material-ui/icons/ImportContacts'
import {connect} from 'react-redux';
// style
import bottomNavigationStyle from "assets/jss/material-kit-react/components/bottomNavigation.jsx";

class SimpleBottomNavigation extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      value: 0,
    };
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes } = this.props;
    const { value } = this.state;
      return (
          <div className={classes.container}>
            <Paper className={classes.paper}>
              <BottomNavigation
                value={value}
                onChange={this.handleChange}
                showLabels
                className={classes.root}
              >
              <Link to="/" className={classes.link}>
                <BottomNavigationAction className={classes.bottomNavAction} label="Home" icon={<HomeIcon />} />
              </Link>
              <Link to="landing-page" className={classes.link}>
                <BottomNavigationAction className={classes.bottomNavAction} label="TravelPlan" icon={<ImportContacts />} />
              </Link>
              <Link to="/" className={classes.link}>
                <BottomNavigationAction className={classes.bottomNavAction} label="Inbox" icon={<MailOutline />}/>
              </Link>
              <Link to="/user/profile" className={classes.link}>
                <BottomNavigationAction className={classes.bottomNavAction} label="Propfile" icon={<AccountCircle />}/>
              </Link>
              </BottomNavigation>
            </Paper>
          </div>
      );
    }
  }

SimpleBottomNavigation.propTypes = {
  classes: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};

// export default withStyles(bottomNavigationStyle)(SimpleBottomNavigation);
export default compose(
  withStyles(bottomNavigationStyle, {
    name: 'SimpleBottomNavigation',
  }),
  connect(null,{}),
)(SimpleBottomNavigation);
