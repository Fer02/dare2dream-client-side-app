import React from "react";
import PropTypes from "prop-types";
import Downshift from "downshift";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import MenuItem from "@material-ui/core/MenuItem";
import InputAdornment from '@material-ui/core/InputAdornment';
import Search from "@material-ui/icons/Search";
import { Link } from "react-router-dom";

function renderInput(inputProps) {
  const { InputProps, classes, ref,locations, ...other } = inputProps;
  return (
    <TextField
      InputProps={{
        inputRef: ref,
        classes: {
          root: classes.inputRoot
        },
        ...InputProps
      }}
      {...other}
    />
  );
}

function renderSuggestion({
  suggestion,
  index,
  itemProps,
  highlightedIndex,
  selectedItem
}) {
  const isHighlighted = highlightedIndex === index;
  const isSelected = (selectedItem || "").indexOf(suggestion.name) > -1;
  return (
    <Link to={{pathname:'/search'+suggestion.name, state:{locationId:suggestion.id}}}>
      <MenuItem
        {...itemProps}
        key={suggestion.id}
        selected={isHighlighted}
        component="div"
        style={{
          fontWeight: isSelected ? 500 : 400
        }}
      >
        {suggestion.name}
      </MenuItem>
    </Link>

  );
}
renderSuggestion.propTypes = {
  highlightedIndex: PropTypes.number,
  index: PropTypes.number,
  itemProps: PropTypes.object,
  selectedItem: PropTypes.string,
  suggestion: PropTypes.shape({ name: PropTypes.string }).isRequired
};

function getSuggestions(items,inputValue) {
  let count = 0;
  return items.filter(suggestion => {
    const keep =
      (!inputValue ||
        suggestion.name.toLowerCase().indexOf(inputValue.toLowerCase()) !==
          -1) &&
      count < 5;
    if (keep) {
      count += 1;
    }
    return keep;
  });
}

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  container: {
    flexGrow: 1,
    position: "relative"
  },
  paper: {
    position: "absolute",
    zIndex: 1200,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`
  },
  inputRoot: {
    flexWrap: "wrap"
  },
  divider: {
    height: theme.spacing.unit * 2
  }
});

function AutoCompleteSearchBar(props) {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <Downshift id="downshift-simple">
        {({
          getInputProps,
          getItemProps,
          isOpen,
          inputValue,
          selectedItem,
          highlightedIndex
        }) => (
          <div className={classes.container}>
            <form className={classes.form}>
              {renderInput({
                fullWidth: true,
                classes,
                InputProps: getInputProps({
                  placeholder: "Masukkan lokasi tujuan wisatamu",
                  endAdornment: <InputAdornment position="end"><Search/></InputAdornment>,
                })
              })}
              {isOpen ? (
                <Paper className={classes.paper} square>
                  {getSuggestions(props.locations,inputValue).map((suggestion, index) =>
                    renderSuggestion({
                      suggestion,
                      index,
                      itemProps: getItemProps({ item: suggestion.name }),
                      highlightedIndex,
                      selectedItem
                    })
                  )}
                </Paper>
              ) : null}
            </form>
          </div>
        )}
      </Downshift>
    </div>
  );
}

AutoCompleteSearchBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AutoCompleteSearchBar);
