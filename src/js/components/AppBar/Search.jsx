import React from "react";
import compose from 'recompose/compose';
import {connect} from 'react-redux';
// @material-ui/core components
import Paper from "@material-ui/core/Paper"
import withStyles from "@material-ui/core/styles/withStyles";
// core components
// import Button from "js/components/material-ui-react/CustomButtons/Button.jsx";
// @material-ui/icons
import AutoCompleteSearchBar from "js/components/AppBar/AutoCompleteSearchBar"
import searchBarHeaderStyle from "assets/jss/material-kit-react/components/searchBarHeaderStyle.jsx";
import {getLocations} from 'js/actions/locationActions'

class SearchBar extends React.Component {

  componentWillMount(){
    this.props.getLocations();
  }

  render() {
    const {classes}=this.props;
    return(
      <div>
        <Paper className={classes.paperSearch}>
            <AutoCompleteSearchBar locations={this.props.locations}/>
        </Paper>
      </div>
    );
  }
}

const mapStateToProp = state => ({
    locations: state.locations.locations,
});


export default compose(
  withStyles(searchBarHeaderStyle, {
    name: 'SearchBar',
  }),
  connect(mapStateToProp,{
    getLocations
  }),
)(SearchBar);
