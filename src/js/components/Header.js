import React, { Component } from 'react';
import logo from '../../logo.svg';
import Title from './header/Title.js'

class Header extends Component {
  handleChange(e){
    const title = e.target.value;
    this.props.changeTitle(title);
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <Title title={this.props.title}/>
          <input value={this.props.title} onChange={this.handleChange.bind(this)}/>
        </header>
      </div>
    );
  }
}

export default Header;
