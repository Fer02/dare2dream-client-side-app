/*eslint-disable*/
import React,{Component} from "react";
// react components for routing our app without refresh
import { Link } from "react-router-dom";
import compose from 'recompose/compose';
import {connect} from 'react-redux';
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";
import Layout from 'js/components/Layout';
import Icon from "@material-ui/core/Icon";
import Hidden from '@material-ui/core/Hidden';
// @material-ui/icons
import {ExitToApp,Settings,Favorite} from "@material-ui/icons";

// core components
import CustomDropdown from "js/components/material-ui-react/CustomDropdown/CustomDropdown.jsx";
import Button from "js/components/material-ui-react/CustomButtons/Button.jsx";

import headerLinksStyle from "assets/jss/material-kit-react/components/headerLinksStyle.jsx";
import { logOutUser } from 'js/actions/sessionActions';
import profile from "assets/img/faces/default-avatar.jpg";

class HeaderLinks extends Component {
  constructor(){
    super()
    this.onClick=this.onClick.bind(this)
  }

  onClick(e){
    e.preventDefault;
    this.props.logOutUser();
  }

  render(){
    const { classes } = this.props;
    return (
      <div>
      <Hidden only={['md','lg','xl']}>
      <div>
      <List className={classes.list}>
        <ListItem className={classes.listItem}>
          <CustomDropdown
            left
            caret={false}
            // hoverColor="primary"
            buttonText={
              <div>
              <img
                src={profile}
                className={classes.img}
                alt="profile"
              />
              <p className={classes.ProfileName}>Nama Profile</p>
              <p className={classes.ProfileEmail}>email</p>
              </div>
            }
            buttonProps={{
              className:
                classes.navLink + " " + classes.imageDropdownButton,
              color: "transparent"
            }}
            dropdownList={[
              "My Profile",
              "Sign out"
            ]}
          />
        </ListItem>
        <ListItem className={classes.listItem}>
        <div className={classes.buttonListItem}>
          <Button
            href="#pablo"
            className={classes.navLink}
            onClick={e => e.preventDefault()}
            color="transparent"
          >
            <Icon className={classes.icons}>import_contacts</Icon><p className={classes.buttonTextStyle
            }>
              Travelplan
              </p>
          </Button>
        </div>
        </ListItem>
        <ListItem className={classes.listItem}>
        <div className={classes.buttonListItem}>
          <Button
            href="#pablo"
            className={classes.navLink}
            onClick={e => e.preventDefault()}
            color="transparent" 
          >
            <Icon className={classes.icons}>mail_outline</Icon><p className={classes.buttonTextStyle
            }>
              Inbox
              </p>
          </Button>
        </div>
        </ListItem>
      </List>
      </div>
      </Hidden>

    {/* Mobile */}
      <Hidden only={['sm','xs']}>
      <div>
      <List className={classes.list}>
        <ListItem className={classes.listItem}>
        <div className={classes.buttonListItem}>
          <Button
            href="#pablo"
            className={classes.navLink}
            onClick={e => e.preventDefault()}
            color="transparent"
          >
            <Icon className={classes.icons}>import_contacts</Icon><p className={classes.buttonTextStyle
            }>
              Travelplan
              </p>
          </Button>
        </div>
        </ListItem>
        <ListItem className={classes.listItem}>
        <div className={classes.buttonListItem}>
          <Button
            href="#pablo"
            className={classes.navLink}
            onClick={e => e.preventDefault()}
            color="transparent" 
          >
            <Icon className={classes.icons}>mail_outline</Icon><p className={classes.buttonTextStyle
            }>
              Inbox
              </p>
          </Button>
        </div>
        </ListItem>
        <ListItem className={classes.listItem}>
          <CustomDropdown
            left
            caret={false}
            hoverColor="primary"
            buttonText={
              <div>
              <img
                src={profile}
                className={classes.img}
                alt="profile"
              />
              </div>
            }
            buttonProps={{
              className:
                classes.navLink + " " + classes.imageDropdownButton,
              color: "transparent"
            }}
            dropdownList={[
              "My Profile",
              "Sign out"
            ]}
          />
        </ListItem>
      </List>
      </div>
      </Hidden>
      </div>

      
      
      
      
      // <List className={classes.list}>
      //   <ListItem className={classes.listItem}>
      //     <Button
      //       href={"/#/register"}
      //       color="transparent"
      //       className={classes.navLink}
      //     >
      //       <Favorite className={classes.icons} /> TravelWish
      //     </Button>
      //   </ListItem>
      //   <ListItem className={classes.listItem}>
      //     <Button
      //       href={"/#/register"}
      //       color="transparent"
      //       className={classes.navLink}
      //     >
      //       <Settings className={classes.icons} /> Pengaturan
      //     </Button>
      //   </ListItem>
      //   <ListItem className={classes.listItem}>
      //     <Button
      //       color="transparent"
      //       className={classes.navLink}
      //       onClick={this.onClick}
      //     >
      //       <ExitToApp className={classes.icons} /> Keluar
      //     </Button>
      //   </ListItem>
      // </List>
    );
  }
}

export default compose(
  withStyles(headerLinksStyle, {
    name: 'HeaderLinks',
  }),
  connect(null,{logOutUser}),
)(HeaderLinks);
