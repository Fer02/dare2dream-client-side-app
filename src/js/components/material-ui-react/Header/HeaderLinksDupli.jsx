/*eslint-disable*/
import React from "react";
// react components for routing our app without refresh
import { Link } from "react-router-dom";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";
import Layout from 'js/components/Layout';
// @material-ui/icons
import {ExitToApp,Settings,Favorite} from "@material-ui/icons";

// core components
import CustomDropdown from "js/components/material-ui-react/CustomDropdown/CustomDropdown.jsx";
import Button from "js/components/material-ui-react/CustomButtons/Button.jsx";

import headerLinksStyle from "assets/jss/material-kit-react/components/headerLinksStyle.jsx";
import { logOutUser } from 'js/actions/sessionActions';

// function logOut() {
//   this.props.logOut()
// }

function HeaderLinks({ ...props }) {
  const { classes } = props;
  return (
    <List className={classes.list}>
      <ListItem className={classes.listItem}>
        <Button
          href={"/#/register"}
          color="transparent"
          className={classes.navLink}
        >
          <Favorite className={classes.icons} /> TravelWish
        </Button>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Button
          href={"/#/register"}
          color="transparent"
          className={classes.navLink}
        >
          <Settings className={classes.icons} /> Pengaturan
        </Button>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Button
          color="transparent"
          className={classes.navLink}
          onClick={this.onClick}
        >
          <ExitToApp className={classes.icons} /> Keluar
        </Button>
      </ListItem>
    </List>
  );
}

export default compose(
  withStyles(headerLinksStyle, {
    name: 'HeaderLinks',
  }),
  connect(null,{logOutUser}),
)(HeaderLinks);
