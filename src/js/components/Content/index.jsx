import React, { Component } from 'react';
import compose from 'recompose/compose';
import { Link } from "react-router-dom";
// import PropTypes from 'prop-types';
// content section
import Category from "js/components/Content/sections/Category.jsx";
import Cards from "js/components/Content/sections/Cards.jsx";
import withStyles from "@material-ui/core/styles/withStyles";
import GridContainer from "js/components/material-ui-react/Grid/GridContainer.jsx";
import GridItem from "js/components/material-ui-react/Grid/GridItem.jsx";
// style
import contentStyle from "assets/jss/material-kit-react/views/contentStyle.jsx";
import {getPlaces,getShortedPlaces} from 'js/actions/placeActions'
//redux
import {connect} from 'react-redux';

class Content extends Component {
  constructor(){
    super();
    this.state ={
      activecategory:0
    };
  }

  placeList(sortedBy,categoryName) {
    this.setState({activecategory:categoryName});
    this.getMostRiviewedList(categoryName);
    this.getMostPopularList(categoryName);
    this.props.getPlaces(sortedBy,categoryName);
  }


  getMostRiviewedList(category){
    this.props.getShortedPlaces("riviewedBy",category);
  }

  getMostPopularList(category){
    this.props.getShortedPlaces("rating",category);
  }

  componentDidMount(){
    this.placeList();
    this.getMostPopularList();
    this.getMostRiviewedList();
  }

  render() {
    let imagePlace;
    const { classes } = this.props;

    return (
      <div>
        <GridContainer>
          <GridItem xs={12}>
            <Category placeList={this.placeList.bind(this)}/>
          </GridItem>
          <GridItem xs={12}>
            <h2 className={classes.title}>Tempat Wista Populer</h2>
            <Link to={{pathname:'/place/list/popular place',state:{locationId:""}}}>
                <p className={classes.allPlace}>Lihat Semua</p>
            </Link>
          </GridItem>
          <GridItem xs={12}>
            <div className={classes.bodyContent}>
              {
                this.props.popularplace.map((place)=>{
                  imagePlace=""
                  place.photos.map((prop) => {
                    return imagePlace=prop.image
                  })
                  return <Cards placeId={place.id} placeName={place.name} ratings={place.rating} images={imagePlace} riviewed={place.riviewedBy}/>
                })
              }
            </div>
          </GridItem>
          <GridItem xs={12}>
            <h2 className={classes.title}>Paling Banyak Diriview</h2>
                <Link to={{pathname:'/place/list/most reviewed place',state:{locationId:""}}}>
                  <p className={classes.allPlace}>Lihat Semua</p>
              </Link>
          </GridItem>
          <GridItem xs={12}>
            <div className={classes.bodyContent}>
              {
                this.props.mostreviewed.map((place)=>{
                  place.photos.map((prop) => {
                    imagePlace=""
                    return imagePlace=prop.image
                  })
                  return   <Cards placeId={place.id} placeName={place.name} ratings={place.rating} images={imagePlace} riviewed={place.riviewedBy}/>
                })
              }
            </div>
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

const mapStateToProp = state => ({
    mostreviewed: state.places.mostreviewed,
    popularplace: state.places.popularplace
});

// export default withStyles(contentStyle)(Content);
export default compose(
  withStyles(contentStyle, {
    name: 'Content',
  }),
  connect(mapStateToProp,{
    getPlaces,getShortedPlaces
  }),
)(Content);
