import React from 'react'
import compose from 'recompose/compose';
import {connect} from 'react-redux';
// materia-ui
// import Grid from '@material-ui/core/Grid'
import withStyles from "@material-ui/core/styles/withStyles";
// react core
import NavPills from "js/components/material-ui-react/NavPills/NavPills.jsx";
import GridContainer from "js/components/material-ui-react/Grid/GridContainer.jsx";
import GridItem from "js/components/material-ui-react/Grid/GridItem.jsx";
// style
import pillsStyle from "assets/jss/material-kit-react/views/componentsSections/pillsStyle.jsx";
import {getCategories} from 'js/actions/placeActions'

class Category extends React.Component{

  componentWillMount() {
    this.props.getCategories();
  }

  getPlaceList(categoryName){
    this.props.placeList(null,categoryName);
  }

  render() {
    const { classes } = this.props;

    const placeCategories = this.props.categories.map(function(category){
      // let icon=<img src={"http://127.0.0.1:8080/static/img/categories/"+place.icon} alt="..." />
      let cat={
        tabButton:category.name,
        tabIcon: category.icon,
        iconId: category.id
      }
      return cat
    });

    return (
      <div className={classes.section}>
        <div className={classes.container}>
          <div id="navigation-pills" >
            <GridContainer className={classes.main}>
              <GridItem xs={12}>
                <NavPills
                  className={classes.navTab}
                  color="info"
                  tabs={placeCategories}
                  getPlaceList={this.getPlaceList.bind(this)}
                />
              </GridItem>
            </GridContainer>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  categories:state.places.categories
});

// export default withStyles(pillsStyle)(Category);
export default compose(
  withStyles(pillsStyle, {
    name: 'Category',
  }),
  connect(mapStateToProps,{
    getCategories
  }),
)(Category);
