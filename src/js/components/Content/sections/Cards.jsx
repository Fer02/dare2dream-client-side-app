import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { Link } from "react-router-dom";
// material-ui components
import withStyles from "@material-ui/core/styles/withStyles";
import Favorite from '@material-ui/icons/Favorite';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import StarRatingComponent from 'react-star-rating-component';
// core components
import Card from "js/components/material-ui-react/Card/Card.jsx";
import CardBody from "js/components/material-ui-react/Card/CardBody.jsx";

import image from "assets/img/bg3.jpg"
import cardContent from "assets/jss/material-kit-react/views/componentsSections/cardContent.jsx"
import Checkbox from '@material-ui/core/Checkbox';
import {apiconfig} from 'js/api/apiconfig'

class Cards extends React.Component {
  constructor(props){
    super(props)
    this.state={
      checkedLike:false
    }
  }

  // handleChange = event => {
  //     this.setState({[event.target.value]:event.target.checked})
  //
  //     const param = {
  //       place_id: this.props.placeId,
  //       like: event.target.checked,
  //     };
  //
  //     this.props.createUser(user,this.props.history);
  //     console.log(event.target.checked)
  // };

  render() {
    const {classes} = this.props;
    let imagePlace;
    if(this.props.images){
        imagePlace=<img src={apiconfig.base_url("static/img/place/")+this.props.images} alt="img-place" className={classes.imgCardTop} />
      }else{
        imagePlace=<img src={image}  alt="Card-img-cap" className={classes.imgCardTop} />
    }
    return (

        <div className={classes.section}>
            <Card className={classes.card}>
              <Link to={{pathname:'/detail/'+this.props.placeName, state:{placeId:this.props.placeId}}} className={classes.link}>
                {imagePlace}
              </Link>
            <CardBody className={classes.cardBody}>
              <Link to={{pathname:'/detail/'+this.props.placeName, state:{placeId:this.props.placeId}}} className={classes.link}>
                <h5 className={classes.cardTitle}>{this.props.placeName}</h5>
                <div className={classes.bodyContent}>
                    <StarRatingComponent
                    name="rate2"
                    editing={false}
                    starCount={5}
                    value={this.props.ratings}
                  />
                  <p className={classes.ulasan}>{this.props.riviewed} ulasan</p>
                </div>
              </Link>
                <FormControlLabel
                   className={classes.cardButton}
                   control={
                     <Checkbox onChange={this.handleChange} checked={this.state.checkedLike} icon={<FavoriteBorder />} checkedIcon={<Favorite />} value="checkedLike" />
                   }
                 />
              </CardBody>
            </Card>
        </div>
    );
  }
}

export default withStyles(cardContent)(Cards);
