import {types} from './types';
import userApi from 'js/api/userApi'

export const createUser = postData => dispatch =>{
  userApi.register(postData).then(user=>dispatch({
      type:types.NEW_USER_FULFILLED,
      payload:user
    })
  );
};
