import {types} from './types';
import locationApi from 'js/api/locationApi'

export const getLocations = () => dispatch => {
  locationApi.getLocationList().then( categories =>dispatch({
      type:types.LOCATION_FULFILLED,
      payload:categories
    })
  );
};
