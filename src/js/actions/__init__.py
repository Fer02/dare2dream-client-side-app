# -*- coding: utf-8 -*-
__version__ = '0.1'
from flask import Flask,Blueprint
from flask_sqlalchemy import SQLAlchemy
from flask_security import Security, SQLAlchemyUserDatastore,SQLAlchemySessionUserDatastore, \
    login_required
from database import db_session, init_db
from admin.models.Users import Users,Roles
from admin.models.Places import Places
from admin.models.Categories import Categories
from admin.models.Riviews import Riviews
from flask_restplus import Api,Resource
from flask_cors import CORS

# Create app
app = Flask(__name__)
CORS(app)
app.config.from_pyfile('../config.py')
# Setup Flask-Security
db = SQLAlchemy(app)
user_datastore = SQLAlchemySessionUserDatastore(db_session,
                                                Users, Roles)
security = Security(app, user_datastore)

app.debug = True
# toolbar = DebugToolbarExtension(app)
from admin.controllers import *
