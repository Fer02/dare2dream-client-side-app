import {types} from './types';
import reviewApi from 'js/api/reviewApi'

export const postReviews = (param) => dispatch => {
  reviewApi.reviews(param).then( response =>dispatch({
      type:types.POST_REVIEW_FULFILLED,
      payload:response
    })
  );
};
