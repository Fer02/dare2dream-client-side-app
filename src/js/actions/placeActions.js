import {types} from './types';
import placeApi from 'js/api/placeApi'

export const getPlaces = (sortedBy,categoryName,location,start) => dispatch => {
  console.log(sortedBy)
  console.log(location)
  let param="*";
  let category="*";
  if(categoryName){
      category=categoryName
  }
  console.log(location)
  console.log(sortedBy)
  if(sortedBy){
      param=sortedBy
      console.log(param)
      placeApi.getPlaceList(param,category,location,start).then( place =>dispatch({
          type:types.PLACE_FILTER_FULFILLED,
          payload:place,
          param:param,
        })
      );
  }else{
    placeApi.getPlaceList(param,category,location,start).then( place =>dispatch({
        type:types.PLACE_FULFILLED,
        payload:place
      })
    );
  }
};

export const getShortedPlaces = (sortedBy,categoryName,location,start) => dispatch => {
  console.log(sortedBy)
  console.log(location)
  let param="*";
  let category="*";
  if(categoryName){
      category=categoryName
  }
  if(sortedBy==="rating"){
      param=sortedBy
      console.log(param)
      placeApi.getPlaceList(param,category,location,start).then( place =>dispatch({
          type:types.POPULAR_PLACE_FULFILLED,
          payload:place,
          param:param,
        })
      );
  }else{
    param=sortedBy
    placeApi.getPlaceList(param,category,location,start).then( place =>dispatch({
        type:types.MOSTREVIEWED_PLACE_FULFILLED,
        payload:place
      })
    );
  }
};

export const getMorePlaces = (nextUrl,prevUrl,location) => dispatch => {
  console.log(nextUrl)
  console.log(prevUrl)
    placeApi.getMorePlaceList(nextUrl,prevUrl,location).then( place =>dispatch({
        type:types.LOADMORE_PLACE_FULFILLED,
        payload:place,
      })
    )
};

export const getRecommendationPlaces = (sortedBy,categoryName,location) => dispatch => {
  let param="*";
  let category="*";
  console.log(location)
  if(categoryName){
      category=categoryName
  }
  if(sortedBy){
      param=sortedBy
      placeApi.getRecommendationList(param,category,location).then( place =>dispatch({
          type:types.PLACE_RECOMMENDATION_FULFILLED,
          payload:place,
          param:param,
        })
      );
  }else{
    placeApi.getRecommendationList(param,category,location).then( place =>dispatch({
        type:types.PLACE_RECOMMENDATION_FULFILLED,
        payload:place
      })
    );
  }
};

export const getCategories = () => dispatch => {
  placeApi.getCategoriesList().then( categories =>dispatch({
      type:types.PLACE_CATEGORIES_FULFILLED,
      payload:categories
    })
  );
};

export const getDetailPlace = (param) => dispatch => {
  placeApi.getDetailPlace(param).then( detail =>dispatch({
      type:types.PLACE_DETAIL_FULFILLED,
      payload:detail
    })
  );
};

export const likes = (param) => dispatch => {
  placeApi.like(param).then( res =>dispatch({
      type:types.PLACE_LIKED_FULFILLED,
      payload:res
    })
  );
};
