import {types} from './types';
// import { push } from 'react-router-redux';

import sessionApi from 'js/api/sessionApi'

export const loginUser = postData => dispatch => {
  const request=sessionApi.login(postData)
    request.then(
      response=>
      dispatch({
        type:types.USER_LOGIN_FULFILLED,
        payload:response
      })
    ).catch(error=>
      dispatch({
        type:types.USER_LOGIN_REJECTED,
        payload: error
      })
    )
};

export const logOutUser = () => {
  return{
    type:types.CHANGE_LOGIN,
    payload:false
  }
};

export const checkLogin = () => {
  var jwt = localStorage.getItem('jwt')
  console.log(localStorage)
  return ({
      type: "CHANGE_LOGIN",
      payload: jwt != null ? true : false
  })
};
