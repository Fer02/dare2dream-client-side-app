workbox.setConfig({ debug: true });
// See https://developers.google.com/web/tools/workbox/guides/configure-workbox
workbox.core.setLogLevel(workbox.core.LOG_LEVELS.debug);
self.addEventListener('install', event => event.waitUntil(self.skipWaiting()));
self.addEventListener('activate', event => event.waitUntil(self.clients.claim()));

// We need this in Webpack plugin (refer to swSrc option): https://developers.google.com/web/tools/workbox/modules/workbox-webpack-plugin#full_injectmanifest_config
workbox.precaching.precache(self.__precacheManifest);

// app-shell
workbox.routing.registerRoute("/", workbox.strategies.networkFirst());

workbox.routing.registerRoute("/static/",workbox.strategies.networkFirst());

workbox.routing.registerRoute(
  new RegExp('http://192.168.1.35/api/'),
  workbox.strategies.staleWhileRevalidate()
);

workbox.routing.registerRoute(
  new RegExp('http://192.168.1.35/api/place/'),
  workbox.strategies.staleWhileRevalidate()
);

workbox.routing.registerRoute(
  new RegExp('http://192.168.1.35/api/place/category'),
  workbox.strategies.staleWhileRevalidate()
);

workbox.routing.registerRoute(
  new RegExp('http://192.168.1.35/api/review/'),
  workbox.strategies.staleWhileRevalidate()
);

// const cacheable = new workbox.cacheableResponse.CacheableResponse({
//   statuses: [0, 200],
// });
//
// const response = await fetch('');
//
// if (cacheable.isResponseCacheable(response)) {
//   const cache = await caches.open('api-cache');
//   cache.put(response.url, response);
// } else {
//   // Do something when the response can't be cached.
// }
