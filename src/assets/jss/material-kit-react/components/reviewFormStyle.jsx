import blue from '@material-ui/core/colors/blue';

const reviewFormStyle = theme => ({
  main:{
    "& button":{
      float:"right",
      [theme.breakpoints.down('sm')]: {
          marginRight:"10px",
          marginTop:"10px"
      },
    },
    paddingBottom:"50px"
  },
  button:{
    color: theme.palette.getContrastText(blue[500]),
    backgroundColor: blue[500],
    '&:hover': {
      backgroundColor: blue[700],
    },
    textTransform:"capitalize"
  },
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    top:"50%",
    left:"50%",
    transform: `translate(-50%, -50%)`,
    [theme.breakpoints.down('sm')]: {
        height:"100%",
    },
    "& button": {
      float: "right"
    }
  },
  reviewContent:{
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent:'center',
    paddingTop:'50px',
    paddingBottom:'50px'
  },
  textField:{
    float:"left",
    width: 300,
  },
  rating:{
    height:"200px",
  }
});

export default reviewFormStyle;
