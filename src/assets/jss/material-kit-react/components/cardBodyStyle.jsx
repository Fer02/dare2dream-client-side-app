const cardBodyStyle = theme=>({
  cardBody: {
    padding: "0.9375rem 1.875rem",
    flex: "1 1 auto",
    [theme.breakpoints.down('sm')]: {
        padding:"0 0 0 20px"
    },
  }
});

export default cardBodyStyle;
