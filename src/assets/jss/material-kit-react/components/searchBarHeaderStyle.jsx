import {
} from "assets/jss/material-kit-react.jsx";

const searchBarHeaderStyle = theme=>({
  formControl: {
    margin: "0 !important",
    paddingTop: "5px",
    width:"100%"
  },
  searchInput: {
    margin: "0px !important",
    padding:"5px",
  },
  inputRootCustomClasses: {
    margin: "10px!important"
  },
  paperSearch:{
    width:"100%",
    marginLeft:"auto",
    marginRight:"auto",
    [theme.breakpoints.down('sm')]: {
        width:"80%",
   },
  }
});

export default searchBarHeaderStyle
