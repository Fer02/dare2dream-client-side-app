const bottomNavigationStyle = {
  container:{
    paddingTop:"55px",
  },
  paper:{
    marginBottom: "0",
    padding: "0",
    bottom:"0",
    width:"100%",
    position: "fixed",
    boxShadow:
      "0 4px 18px 0px rgba(0, 0, 0, 0.12), 0 7px 10px 10px rgba(0, 0, 0, 0.15)",
  },
  bottomNavAction:{
    "&:hover": {
      color: "#2196f3",
      background: "rgba(200, 200, 200, 0.2)"
    }
  },
  link:{
    color:"#000000",
    margin:"0 auto 0 auto"
  }
}

export default bottomNavigationStyle
