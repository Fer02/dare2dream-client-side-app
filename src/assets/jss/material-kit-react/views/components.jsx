import { container } from "assets/jss/material-kit-react.jsx";

const componentsStyle = theme=>({
  container,
  brand: {
    color: "#FFFFFF",
    textAlign: "center",
  },
  title: {
    fontSize: "4.2rem",
    fontWeight: "600",
    display: "inline-block",
    position:"relative",
    [theme.breakpoints.down('sm')]: {
     fontSize:"1.8rem",
     paddingBottom:"10px"
   },
  },
  subtitle: {
    fontSize: "1.913rem",
    marginTop:"-30px",
    [theme.breakpoints.down('sm')]: {
     fontSize:"0.9rem",
   },
  },
  main: {
    background: "#fff",
    position: "relative",
    zIndex: "1"
  },
  mainRaised: {
    margin: "-60px auto 30px auto",
    borderRadius: "6px",
  },
  link: {
    textDecoration: "none"
  },
  textCenter: {
    textAlign: "center"
  },
  content:{
    position:"relative",
    width:"100%",
  }
});

export default componentsStyle;
