const contentStyle = theme=>({
  container:{
    paddingBottom:"30px",
  },
  title:{
    marginLeft:"20px",
    fontWeight:"bold",
    [theme.breakpoints.down('sm')]: {
        marginLeft:"20px",
        fontSize:"30px",
    },
  },
  bodyContent:{
    width: "auto",
    overflowX: "auto",
    whiteSpace: "nowrap",
    padding:"10px 0 10px 0"
  },
  allplacebodyContent:{
    width: "auto",
    padding:"30px 0 10px 0",
  },
  subHeader:{
    padding:"0 0 0 20px"
  },
  button:{
    margin:"15px auto 0 auto",
    alignItem:"center",
    display:"flex",
    width:"30%"
  },
  allPlace:{
    float:"right",
    marginRight:"10px",
    marginBottom:"-5px",
    fontWeight:900,
    color:"#2196f3"
  }
});
export default contentStyle;
