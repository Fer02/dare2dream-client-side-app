import {title } from "assets/jss/material-kit-react.jsx";

const pillsStyle = {
  container:{
    width:"100%",
    padding: "70px 0 0 0",

  },
  title: {
    ...title,
    marginTop: "30px",
    minHeight: "32px",
    textDecoration: "none"
  },
  main:{
    width:"100%",
    margin:"0 auto 0 auto",
  },
};

export default pillsStyle;
