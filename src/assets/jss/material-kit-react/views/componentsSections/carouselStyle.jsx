import { container } from "assets/jss/material-kit-react.jsx";

const carouselStyle = {
  section: {
    padding: "30px 0"
  },
  container,
  marginAuto: {
    marginLeft: "auto !important",
    marginRight: "auto !important",
    width:"100%"
  }
};

export default carouselStyle;
