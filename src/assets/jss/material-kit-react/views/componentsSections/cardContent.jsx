import { container} from "assets/jss/material-kit-react.jsx";
import imagesStyles from "assets/jss/material-kit-react/imagesStyles.jsx";

const cardContent=theme=>({
  ...imagesStyles,
  section:{
    padding:"-20px 0 10px 0",
    display:"inline-block",
    whiteSpace:"normal",
    paddingBottom:"25px"
  },
  container:{
    ...container,
  },
  card:{
    width: "20rem",
    margin:"0 5px 0 10px",
    [theme.breakpoints.down('sm')]: {
        width:"10rem",
        maxHeight:"270px",
        marginBottom:"20px"
    },
  },
  cardBody:{
    padding:"0 0 0 20px"
  },
  cardTitle:{
    fontWeight: "bold",
    display:"wrap",
    textTransform:"capitalize",
    fontSize:"15px",
    maxHeight:"29px",
    [theme.breakpoints.down('sm')]: {
        fontSize:"12px",
        maxHeight:"19px",
        overflow: "hidden"
    }
  },
  cardButton:{
    float:"right",
  },
  bodyContent:{
    padding:"0",
    height:"20px",
    lineHeight:"normal",
  },
  imgCardTop:{
    height:"150px",
    width:"100%",
    display:"block",
    [theme.breakpoints.down('sm')]: {
        width:"100%",
        height:"120px",
    }
  },
  ratings:{
    marginTop:"-10px"
  },
  ratingsIcon:{
    padding:"0"
  },
  link:{
    color:"#000000",
    "&:hover": {
      color: "#000000",
    }
  }
});
export default cardContent
