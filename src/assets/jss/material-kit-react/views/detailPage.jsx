import {title} from "assets/jss/material-kit-react.jsx";
import imagesStyle from "assets/jss/material-kit-react/imagesStyles.jsx";

const detailPageStyle = theme=>({
  ...imagesStyle,
  container:{
      marginRight:"auto",
      marginLeft:"auto",
  },
  infoSection: {
    textAlign: "left",
    paddingTop:"90px",
    "& img": {
      maxWidth: "160px",
      width: "100%",
      margin: "0 auto",
      transform: "translate3d(0, -50%, 0)",
    },
    [theme.breakpoints.down('sm')]: {
      paddingLeft:"20px",
      paddingRight:"20px",
    }
  },
  description: {
    margin: "1.071rem auto 0",
    maxWidth: "600px",
    color: "#999",
    textAlign: "center !important"
  },
  name: {
    marginTop: "-80px"
  },
  main: {
    background: "#FFFFFF",
    position: "relative",
    marginTop:"50px",
    marginLeft:"auto",
    marginRight:"auto"
  },
  title: {
    ...title,
    position: "relative",
    marginTop: "5px",
    minHeight: "32px",
    textDecoration: "none"
  },
  navWrapper: {
    margin: "20px auto 50px auto",
    textAlign: "center"
  },
  ratingInfo:{
    "& p":{
      display: "inline-block",
      paddingBottom:"0",
      marginBottom:"0",
    },
  },
  placeGalery:{
    width:"100%",
    margin: "0 auto 0 auto"
  },
  mainInfo:{
    paddingRight:"0px",
    [theme.breakpoints.down('sm')]: {
        paddingRight:"0",
    },
  },
  mainInfoReviews:{
    paddingBottom:"30px",
  },
  infoContainer:{
    paddingTop:"60px",
    marginRight:"80px",
    [theme.breakpoints.down('sm')]: {
        marginRight:"0px",
    },
  },
  content:{
    "& p":{
      display: "inline-block",
      paddingBottom:"0",
      marginLeft:"-30px",
      marginBottom:"0",
      [theme.breakpoints.down('sm')]: {
          marginLeft:"0px",
      },
    },

  },
  reviewContent:{
    paddingBottom:"10px",
    paddingTop:"10px"
  }
});

export default detailPageStyle;
