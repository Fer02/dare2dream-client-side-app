import React,{Component} from 'react';
import 'css/index.css';
import HomePage from 'js/pages/HomePage/HomePage';
import LoginPage from 'js/pages/LoginPage/LoginPage';
import RegisterPage from 'js/pages/RegisterPage/RegisterPage';
import ProfilePage from 'js/pages/ProfilePage/ProfilePage';
import DetailUI from 'js/pages/PlaceUI/DetailUI';
import PlaceList from 'js/pages/PlaceUI/PlaceList';
import {Route,Redirect,Router} from "react-router-dom";
import SearchResult from 'js/pages/SearchResult/index.jsx'
import history from 'js/history'
// redux
import {Provider} from 'react-redux';
import store from 'js/store';

import "assets/scss/material-kit-react.css?v=1.1.0";

import {checkLogin} from "js/actions/sessionActions";

class App extends Component {
  constructor(){
    super()
    store.dispatch(checkLogin())
    this.state = {
      isLoggedIn:store.getState().session.loggedIn,
      title:null
    }
    store.subscribe(() =>{
      this.setState ({
        isLoggedIn:store.getState().session.loggedIn,
        title:store.getState().session.title
      })
    })
  }
  render(){
    return (
      <Provider store={store}>
        <Router history={history}>
            <div>
              <Route exact path="/" render={(props)=> (!this.state.isLoggedIn ? <Redirect to='/login' /> : <HomePage{...props}/>)}/>
              <Route path='/login' render={(props) => (this.state.isLoggedIn ? <Redirect to='/' /> : <LoginPage {...props} />)} />
              <Route path="/register" render={(props) => (this.state.isLoggedIn ? <Redirect to='/' /> : <RegisterPage {...props} />)} />
              <Route path="/user/profile" render={(props) => (this.state.isLoggedIn ? <ProfilePage {...props} /> : <Redirect to='/login' />)} />
              <Route path="/detail/:placeName" render={(props) => (this.state.isLoggedIn ? <DetailUI {...props} params={props.params}/> : <Redirect to='/login' />)} />
              <Route path="/search:locationName" render={(props) => (this.state.isLoggedIn ? <SearchResult {...props} params={props.params}/> : <Redirect to='/login' />)} />
              <Route path="/place/list/:type" render={(props) => (this.state.isLoggedIn ? <PlaceList {...props} params={props.params}/> : <Redirect to='/login' />)} />
          </div>
        </Router>
      </Provider>
    )
  }
}

export default App;
